# GeN-Foam Logos

## Standalone logo and name

<img src="GeN-Foam-logo-bold-italic.png" width=500px>

<img src="GeN-Foam-logo-bold-italic.jpg" width=500px>

<img src="GeN-Foam-logo-bold-italic.svg" width=500px>

*Fig 1: Standalone logos and name in PNG, JPG and SVG*


## Standalone logo

<img src="GeN-Foam-logo.png" width=250px>
<img src="GeN-Foam-logo.jpg" width=250px>
<img src="GeN-Foam-logo.svg" width=250px>

*Fig 2: Standalone logos in PNG, JPG and SVG*