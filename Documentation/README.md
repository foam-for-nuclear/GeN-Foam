# Documentation

This folder contains a link to a fairly comprehensive online Doxygen-generated documentation ([link](./doxygen/files/)), as well as a folder with the standard OpenFOAM user guide, an introductory presentation about the logic behind GeN-Foam ([GeN-Foam Theory](./usefulDocumentsAndPresentations/LectureGeNFoam_theory.pdf)), an introduction on how to use GeN-Foam ([GeN-Foam Practice](./usefulDocumentsAndPresentations/LectureGeNFoam_practice.pdf)).
