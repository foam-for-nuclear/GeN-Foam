* [Introduction to GeN-Foam - README file](@ref README)
	* [Compiling GeN-Foam](@ref COMPILE)
	* [Preprocessing](@ref PREPROCESSING)
	* [Running GeN-Foam](@ref RUNNING)
	* [Postprocessing](@ref POSTPROCESSING)


* [User manual](@ref USERMAN)
	* [Neutronics](@ref NEUTRONICS)
	* [Thermal-hydraulics](@ref TH)
	* [Thermal-mechanics](@ref TM)
	* [Coupling and time stepping](@ref COUPLING)
	* [FMU](@ref FMU)


* [Tutorials](@ref TUTORIALS)

* [Tips and tricks](@ref TIPS)

* [Important notes](@ref NOTES)

* [Recent changes in the case folder](@ref CHANGES)


N.B.: While the documentation reported on this page is preliminary but mostly finished, several classes have been created in the last 8 years that still lack proper documentation in the header files. We are working on it...
