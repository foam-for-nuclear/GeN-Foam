# Active Developers

- Carlo Fiorina, TAMU (Texas A&M University), USA
- Alessandro Scolaro, EPFL (Swiss Federal Technology Institute of Lausanne), Switzerland
- Ivor Clifford, PSI (Paul Scherrer Institut), Switzerland
- Edoardo Brunetto, EPFL, Switzerland
- Thomas Guilbaud, EPFL, Switzerland
- Giovanni Nervi, EPFL, Switzerland


## Contributors

Contributors are individually acknowledged in each source file of GeN-Foam.

In alphabetic order:

- Peter German, EPFL, Switzerland
- Ludovic Jantzen, UC Berkeley, USA
- Gauthier Lazare
- Yuhang Niu
- Hao Qin
- Stefan Radman, EPFL, Switzerland
- Yvver Robert, UC Berkeley, USA
- Eymeric Simonnot, EPFL, Switzerland
