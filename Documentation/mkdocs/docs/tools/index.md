# Tools

Several tools have been developed throughout the years and are available to the community under the [Tools](https://gitlab.com/foam-for-nuclear/GeN-Foam/-/tree/master/Tools/) folder.


## Nuclear Data Generation

- [serpentToFoam](https://gitlab.com/foam-for-nuclear/GeN-Foam/-/tree/master/Tools/serpentToFoam/serpent2.1.23) routines provided with GeN-Foam (in the *Tools* folder) is an Octave script that automatically converts Serpent [@LEPPANEN2015142] output files into the nuclear data files employed by GeN-Foam.
- [openmcToFoam](https://gitlab.com/foam-for-nuclear/GeN-Foam/-/tree/master/Tools/openmcToFoam) Python package provided with GeN-Foam automatically converts OpenMC [@ROMANO201590] output into nuclear data files.
