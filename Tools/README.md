# Tools

This folder contains helper tools that have been developed throughout the years by GeN-Foam users to simplify the use of GeN-Foam. They are not necessary tools for GeN-Foam, just tools that can potentially simplify some pre- and post-processing operations. They are not supposed to be general-purpose tools, and they have not been designed with user-friendliness in mind. However, we believe they can be of help to some users.


## Multi-group cross-section formating

| Code    | Type        | Tool | API |
|:--------|:-----------:|:----:|:---:|
| Serpent | Monte Carlo | [serpentToFoam](./serpentToFoam/) | Octave |
| OpenMC  | Monte Carlo | [openmcToFoam](./openmcToFoam/) | Python |


## Plot residuals

Gnuplot script ([residualsPlot](./residualsPlot/)).
