# Nuclear Data Generation from Serpent using Octave

This is an octave script that has been created to automatically convert Serpent output files into GeN-Foam-readable `nuclearData` and perturbed `nuclearData*` files. One should be able to run the script by typing `autoOctaveSerpentToFoamXS` on an octave shell. Input parameters are specified in the `universesToInclude.m` file.

Or using octave in bash:
```bash
octave autoOctaveSerpentToFoamXS.m
```
