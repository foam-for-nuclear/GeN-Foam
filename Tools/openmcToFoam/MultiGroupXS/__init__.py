# __init__.py for the MultiGroupXS package.
# Author: Thomas Guilbaud, 24/10/2022

import openmc

from MultiGroupXS.MultiGroupXS import *
from MultiGroupXS.MultiGroupXSManager import *
