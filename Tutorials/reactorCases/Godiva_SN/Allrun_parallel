#!/bin/bash
#----------------------------------------------------------------------------#
#       ______          _   __           ______                              #
#      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___    #
#     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \   #
#    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /   #
#    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/    #
#    Copyright (C) 2015 - 2022 EPFL                                          #
#----------------------------------------------------------------------------#

cd ${0%/*} || exit 1    # Run from this directory

#==============================================================================*

# Source tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

#==============================================================================*

# Start gnuplot if it exist
if command -v gnuplot &> /dev/null
then
    gnuplot -e "log='log.GeN-Foam'" residuals - &> /dev/null &
else
    echo "gnuplot could not be found"
fi

nCores=4

# Decompose mesh for parallel computation
foamDictionary system/neutroRegion/decomposeParDict -entry numberOfSubdomains -set $nCores
decomposePar -region neutroRegion
# Manually copy pasting uniform folder because only one region
for i in $(seq 0 $(($nCores-1)))
do
    cp -r 0/uniform processor$i/0
done

# Run GeN-Foam in parallel
mpirun -np $nCores GeN-Foam -parallel | tee "log.GeN-Foam"

# Reconstruct the submesh
reconstructPar -region neutroRegion

# Kill gnuplot if it was started
pkill -x gnuplot &> /dev/null &

#==============================================================================*
