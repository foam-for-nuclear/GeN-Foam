/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      multiRegionCouplingDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

multiPhysicsSolvers
{
    energyNeutroMechLoop
    {
        solvers
        {
            fluidRegion     onePhase;
            neutroRegion    pointKinetics;
        }
        minResidual     0.00005;
        maxIterations   3;
    }
}

mappings
{
    fluidRegion
    {
        neutroRegion
        {
            sourceFields    ( powerDensity secondaryPowerDensity );
            targetFields    ( powerDensityNeutronics powerDensityNeutronicsToLiquid );
        }
    }
    neutroRegion
    {
        fluidRegion
        {
            sourceFields    ( T thermo:rho T.fuelAvForNeutronics T.cladAvForNeutronics T.passiveStructure );
            targetFields    ( TCool rhoCool TFuel TClad TStruct );
        }
    }
}


// ************************************************************************* //
