/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      thermophysicalProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

thermoType
{
    type            heRhoThermo;
    mixture         pureMixture;
    transport       polynomial;
    thermo          hPolynomial;
    equationOfState icoPolynomial;
    specie          specie;
    energy          sensibleEnthalpy;
}

pRef            100000;

mixture
{
    specie
    {
        molWeight       33.0; // https://www.osti.gov/servlets/purl/1841023
    }
    equationOfState
    {
        rhoCoeffs<8>    (2413.03 -0.4884 0 0 0 0 0 0); // Molten salts database for energy applications R. Serrano-López
    }
    thermodynamics
    {
        CpCoeffs<8>     (2265.75 0 0 0 0 0 0 0); // Molten salts database for energy applications R. Serrano-López
        Hf              0;
        Sf              0;
    }
    transport
    {
        muCoeffs<8>     (0.001 0 0 0 0 0 0 0); // fitted (R=1, T=800-1000K), Molten salts database for energy applications R. Serrano-López
        kappaCoeffs<8>  (1.1 0 0 0 0 0 0 0); // Molten salts database for energy applications R. Serrano-López
    }
}


// ************************************************************************* //
