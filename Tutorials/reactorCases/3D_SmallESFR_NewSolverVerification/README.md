# 3D Small ESFR - New Solver verification

The geometry and case are the same as those of 3D_SMALLESFR. This Tutorial shows the newly implemented thermomechanics solver, taken from the fuel behaviour code [OFFBEAT](https://gitlab.com/foam-for-nuclear/offbeat).

The steady state solutions of the legacy (old) approach and the new one are compared. The `./Allrun_parallel` script runs both cases sequentially on 4 cores.
