/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
//Options to turn on physics. Both defaulted to "on"

thermalSolver       on; // Option to turn on/off the thermal solution. Might be not needed as TStruct from TH can be mapped
mechanicsSolver     on; // Option to turn on/off mechanics solution

// To mantain continuity with OFFBEAT, in globalOptions general simulation parameters are set. Both entries needed

globalOptions
{
    linkedFuel      false;
    pinDirection    (0 0 1);
}

// To mantain continuity with OFFBEAT. Only rheology option to set is planeStress. Defaulted to off

rheologyOption
{
    planeStress 	no;
}

// What "zones" used to be. Structure changed to be compatible with OFFBEAT. Possible extension to non-constant materials

materials
{
    innerCore
    {
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
        alphaFuel   alphaFuel   [0 0 0 0 0]     1.1e-5;
        TFuelRef    TFuelRef    [0 0 0 1 0]     668;
    }
    outerCore
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
        alphaFuel   alphaFuel   [0 0 0 0 0]     1.1e-5;
        TFuelRef    TFuelRef    [0 0 0 1 0]     668;
    }
    follower
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
    }
    controlRod
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
        TCRRef      TCRRef  [0 0 0 1 0]     668;
        alphaCR     alphaCR   [0 0 0 0 0]   5.4e-5;
    }
    diagrid
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
    }
    radialReflector
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
    }
    rest
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
    }
    softStructure
    {
        material        constant;
        rho         rho     [1 -3 0 0 0]    1000;
        Cp          Cp      [0 2 -2 -1 0]   200;
        k           k       [1 1 3 -1 0]    5;
        emissivity emissivity [0 0 0 0 0]   0.0;
        E           E       [1 -1 -2 0 0]   1e9;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   0.0;
        alpha       alpha   [0 0 0 0 0]     1.8e-5;
        Tref        Tref    [0 0 0 1 0]     668;
    }
}

// ************************************************************************* //
