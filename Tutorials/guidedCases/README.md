# Guided Tutorials

The folder groups simple GeN-Foam cases.

- 1D neutronics
    - Slab reactor in 1 group ([link](./1_reactorSlab_1D_1Gr_neutronicDiffusion/))
    - Slab reactor with reflector in 1 group ([link](./2_reactorSlabReflected_1D_1Gr_neutronicDiffusion/))
    - Slab reactor with reflector in 2 groups (work in progress)
