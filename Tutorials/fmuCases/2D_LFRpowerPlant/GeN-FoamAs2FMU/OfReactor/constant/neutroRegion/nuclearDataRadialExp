/*--------------------------------*- C++ -*----------------------------------*\
|   Cross-section dictionary                                                  |
|   Generated for GeN-Foam by SerpentToFoamXS Version 1.0 - 22/06/2022        |
|   Date: 22/06/2022                                                          |
|   From SERPENT results file: 2D_ALFRED_Critical_res.m                       |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     1.0;
    format      ascii;
    class       dictionary;
    location    "constant/neutroRegion";
    object      nuclearDataRadialExp;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Effective delayed neutron fraction, prompt neutron spectrum

expansionFromNominal      5.0e-2;

radialOrientation         ( 1 0 0 );

axialOrientation          ( 0 0 1 );


//- Parameters relevant only for Diffusion Neutronics

energyGroups              2; // from MACRO_NG

precGroups                1; // from LAMBDA


//- Foam zones from Serpent universes

zones
(/*
	innerFuel  //- Serpent Universe: uInnFuel
	{
		fuelFraction       1;
		IV                 nonuniform List<scalar> 2 ( 3.503780e-07 1.145860e-04 );
		D                  nonuniform List<scalar> 2 ( 1.174270e-02 5.617470e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 5.994770e-01 2.895970e+01 );
		sigmaPow           nonuniform List<scalar> 2 ( 6.811546e-12 3.321972e-10 );
		scatteringMatrixP0 2 2 (
			( 3.464820e+01 3.531190e-06 )
			( 2.022660e+00 4.603870e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 2.674760e+00 -1.188380e-06 )
			( -3.782740e-01 -1.891630e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.241280e+00 3.882600e-08 )
			( -1.343150e-01 7.961580e-01 )
		);
		scatteringMatrixP3 2 2 (
			( 4.705090e-01 -2.239340e-07 )
			( -1.340300e-01 1.708160e+00 )
		);
		scatteringMatrixP4 2 2 (
			( 2.712120e-01 8.927190e-08 )
			( 1.168640e-01 -5.574530e-01 )
		);
		scatteringMatrixP5 2 2 (
			( 7.520850e-02 1.143290e-07 )
			( -1.121650e-01 -3.097660e-01 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.545000e-01 2.171310e+01 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 2.395251e-01 2.394966e-05 );
	}

	outerFuel  //- Serpent Universe: uOutFuel
	{
		fuelFraction       1;
		IV                 nonuniform List<scalar> 2 ( 3.901830e-07 1.315970e-04 );
		D                  nonuniform List<scalar> 2 ( 1.176550e-02 6.157480e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 6.977490e-01 9.034890e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 7.904605e-12 1.038334e-10 );
		scatteringMatrixP0 2 2 (
			( 3.467570e+01 1.080340e-04 )
			( 1.142770e+00 4.639340e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 2.744800e+00 -2.844140e-05 )
			( -1.303740e-01 4.755190e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.320180e+00 -1.063180e-06 )
			( -4.365010e-02 2.177640e-01 )
		);
		scatteringMatrixP3 2 2 (
			( 5.022240e-01 -4.302240e-08 )
			( -1.367410e-02 -3.680310e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 2.925770e-01 -7.457910e-07 )
			( 4.785320e-03 2.046610e-01 )
		);
		scatteringMatrixP5 2 2 (
			( 8.057640e-02 -9.867960e-07 )
			( -2.553540e-02 -1.543040e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 5.224000e-01 8.446000e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 2.435907e-01 1.166448e-03 );
	}

	innerFuelDiagrid  //- Serpent Universe: uDiagrid
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 5.011330e-06 1.309850e-04 );
		D                  nonuniform List<scalar> 2 ( 6.383490e-03 4.839430e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 6.198060e+01 1.718110e-02 )
			( 1.598950e+00 6.425430e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.097540e+00 -4.609660e-03 )
			( -1.571450e-01 9.809920e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.656850e-01 -3.541260e-04 )
			( -2.554110e-02 -2.831980e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.568460e-02 -3.441890e-04 )
			( -6.579230e-02 -2.612620e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 5.796950e-03 1.094200e-04 )
			( -2.581740e-02 -3.511120e-01 )
		);
		scatteringMatrixP5 2 2 (
			( -8.243020e-03 2.151740e-05 )
			( 4.062920e-02 4.367430e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.475000e-01 5.878800e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.882149e-04 4.108015e-04 );
	}

	outerFuelDiagrid  //- Serpent Universe: uDiagrid
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 5.011330e-06 1.309850e-04 );
		D                  nonuniform List<scalar> 2 ( 6.383490e-03 4.839430e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 6.198060e+01 1.718110e-02 )
			( 1.598950e+00 6.425430e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.097540e+00 -4.609660e-03 )
			( -1.571450e-01 9.809920e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.656850e-01 -3.541260e-04 )
			( -2.554110e-02 -2.831980e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.568460e-02 -3.441890e-04 )
			( -6.579230e-02 -2.612620e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 5.796950e-03 1.094200e-04 )
			( -2.581740e-02 -3.511120e-01 )
		);
		scatteringMatrixP5 2 2 (
			( -8.243020e-03 2.151740e-05 )
			( 4.062920e-02 4.367430e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.475000e-01 5.878800e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.882149e-04 4.108015e-04 );
	}

	outerDiagrid  //- Serpent Universe: uDiagrid
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 5.011330e-06 1.309850e-04 );
		D                  nonuniform List<scalar> 2 ( 6.383490e-03 4.839430e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 6.198060e+01 1.718110e-02 )
			( 1.598950e+00 6.425430e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.097540e+00 -4.609660e-03 )
			( -1.571450e-01 9.809920e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.656850e-01 -3.541260e-04 )
			( -2.554110e-02 -2.831980e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.568460e-02 -3.441890e-04 )
			( -6.579230e-02 -2.612620e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 5.796950e-03 1.094200e-04 )
			( -2.581740e-02 -3.511120e-01 )
		);
		scatteringMatrixP5 2 2 (
			( -8.243020e-03 2.151740e-05 )
			( 4.062920e-02 4.367430e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.475000e-01 5.878800e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.882149e-04 4.108015e-04 );
	}

	innerLowerFuelAssemblies  //- Serpent Universe: uInnLowerFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 1.006000e-06 1.177630e-04 );
		D                  nonuniform List<scalar> 2 ( 1.515220e-02 8.092670e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 2.643720e+01 3.314670e-04 )
			( 1.152370e+00 3.941710e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.107720e+00 -8.012510e-05 )
			( -1.456350e-01 8.199870e-02 )
		);
		scatteringMatrixP2 2 2 (
			( 4.956970e-01 -9.157680e-06 )
			( -4.285100e-02 -2.480020e-03 )
		);
		scatteringMatrixP3 2 2 (
			( 1.263210e-01 3.093380e-06 )
			( 2.613670e-03 -1.017750e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 6.748960e-02 -2.829470e-06 )
			( -2.577180e-03 -1.895600e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 9.460820e-03 -5.716920e-06 )
			( -3.168390e-02 -2.536020e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.206000e-01 2.893800e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 4.334559e-02 6.904185e-04 );
	}

	innerUpperFuelAssemblies  //- Serpent Universe: uInnUpperFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 2.097800e-06 1.204670e-04 );
		D                  nonuniform List<scalar> 2 ( 1.127160e-02 6.804810e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 3.527620e+01 2.165190e-03 )
			( 1.321340e+00 4.619110e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.153880e+00 -5.531550e-04 )
			( -1.726010e-01 4.021900e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.754760e-01 -1.892370e-05 )
			( -5.553390e-02 8.447460e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.096030e-01 -1.623190e-05 )
			( -1.436660e-02 2.026890e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.889350e-02 -1.440780e-05 )
			( -1.360960e-02 -1.100920e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 7.385760e-03 -6.516290e-06 )
			( -9.708440e-03 -2.177700e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 8.950000e-02 3.056200e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.546237e-02 7.508892e-03 );
	}

	outerLowerFuelAssemblies  //- Serpent Universe: uOutLowerFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 1.463480e-06 1.267970e-04 );
		D                  nonuniform List<scalar> 2 ( 1.438780e-02 8.291360e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 2.775740e+01 1.801730e-03 )
			( 9.516450e-01 3.788500e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.164950e+00 -4.172280e-04 )
			( -1.356100e-01 2.703800e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 5.095690e-01 -4.678290e-05 )
			( -2.910660e-02 -1.791070e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.308820e-01 -1.114690e-05 )
			( -2.762200e-02 -1.200460e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 7.133170e-02 1.891520e-06 )
			( -9.916730e-03 3.834790e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 7.968610e-03 -5.138660e-06 )
			( 3.165570e-04 5.711540e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 2.605000e-01 2.729400e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 3.571613e-02 3.864767e-03 );
	}

	outerUpperFuelAssemblies  //- Serpent Universe: uOutUpperFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 2.347120e-06 1.212360e-04 );
		D                  nonuniform List<scalar> 2 ( 1.091000e-02 6.896290e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 3.613010e+01 2.778570e-03 )
			( 1.227720e+00 4.570520e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.158820e+00 -6.660590e-04 )
			( -1.425010e-01 4.488840e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.718640e-01 -4.210310e-05 )
			( -6.200110e-02 6.552440e-03 )
		);
		scatteringMatrixP3 2 2 (
			( 1.078910e-01 -4.565320e-05 )
			( -1.772630e-02 2.246380e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.834260e-02 -4.082820e-06 )
			( -1.400270e-02 1.929550e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 9.080380e-03 5.389280e-06 )
			( -1.252610e-02 1.309420e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 9.830000e-02 2.973300e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 8.020215e-02 1.048604e-02 );
	}

	lowerReflectorAssemblies  //- Serpent Universe: uReflector
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 3.653920e-06 1.315740e-04 );
		D                  nonuniform List<scalar> 2 ( 8.969600e-03 6.830180e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 1.869860e-02 4.424480e-02 );
		sigmaPow           nonuniform List<scalar> 2 ( 2.127107e-13 5.088939e-13 );
		scatteringMatrixP0 2 2 (
			( 4.132620e+01 1.179640e-02 )
			( 1.034490e+00 4.639990e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.256960e+00 -2.909740e-03 )
			( -1.231020e-01 6.260560e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.698600e-01 -2.105520e-04 )
			( -4.049450e-02 5.803280e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 9.374580e-02 -6.519270e-05 )
			( -1.628710e-02 2.550850e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.068950e-02 -5.663790e-05 )
			( -8.201560e-03 -2.711380e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 8.655080e-03 -8.318280e-06 )
			( -6.437120e-03 5.981400e-03 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.659000e-01 2.917300e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 1.616689e-01 9.563495e-02 );
	}

	middleReflectorAssemblies  //- Serpent Universe: uReflector
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 3.653920e-06 1.315740e-04 );
		D                  nonuniform List<scalar> 2 ( 8.969600e-03 6.830180e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 1.869860e-02 4.424480e-02 );
		sigmaPow           nonuniform List<scalar> 2 ( 2.127107e-13 5.088939e-13 );
		scatteringMatrixP0 2 2 (
			( 4.132620e+01 1.179640e-02 )
			( 1.034490e+00 4.639990e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.256960e+00 -2.909740e-03 )
			( -1.231020e-01 6.260560e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.698600e-01 -2.105520e-04 )
			( -4.049450e-02 5.803280e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 9.374580e-02 -6.519270e-05 )
			( -1.628710e-02 2.550850e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.068950e-02 -5.663790e-05 )
			( -8.201560e-03 -2.711380e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 8.655080e-03 -8.318280e-06 )
			( -6.437120e-03 5.981400e-03 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.659000e-01 2.917300e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 1.616689e-01 9.563495e-02 );
	}

	upperReflectorAssemblies  //- Serpent Universe: uReflector
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 3.653920e-06 1.315740e-04 );
		D                  nonuniform List<scalar> 2 ( 8.969600e-03 6.830180e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 1.869860e-02 4.424480e-02 );
		sigmaPow           nonuniform List<scalar> 2 ( 2.127107e-13 5.088939e-13 );
		scatteringMatrixP0 2 2 (
			( 4.132620e+01 1.179640e-02 )
			( 1.034490e+00 4.639990e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.256960e+00 -2.909740e-03 )
			( -1.231020e-01 6.260560e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.698600e-01 -2.105520e-04 )
			( -4.049450e-02 5.803280e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 9.374580e-02 -6.519270e-05 )
			( -1.628710e-02 2.550850e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.068950e-02 -5.663790e-05 )
			( -8.201560e-03 -2.711380e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 8.655080e-03 -8.318280e-06 )
			( -6.437120e-03 5.981400e-03 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.659000e-01 2.917300e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 6.682940e-05 5.960520e-04 4.781090e-04 1.215210e-03 4.840720e-04 1.484360e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.270840e-02 3.008530e-02 1.115980e-01 3.239900e-01 1.202450e+00 7.642120e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 1.616689e-01 9.563495e-02 );
	}
*/
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
