/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2112                                  |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    arch        "LSB;label=32;scalar=64";
    class       dictionary;
    location    "system/fluidRegion";
    object      createBafflesDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// [1] Measurements of thermophysical properties of solid and liquid NIST SRM
//      316L stainless steel, Peter Pichler, Brian J. Simonds, Jeffrey W.
//      Sowards, and Gernot Pottlacher, J Mater Sci (2020) 55:4081–4093, 2019

// Whether to convert internal faces only (so leave boundary faces intact).
// This is only relevant if your face selection type can pick up boundary
// faces.
internalFacesOnly   true;

// Baffles to create.
baffles
{
    baffleFaces
    {
        //- Use predefined faceZone to select faces and orientation.
        type        faceZone;
        zoneName    bafflePool;

        patches
        {
            master
            {
                //- Master side patch
                name            bafflePool_master;
                type            mappedWall;
                sampleMode      nearestPatchFace;
                sampleRegion    fluid;
                samplePatch     bafflePool_slave;
                offsetMode      uniform;
                offset          (0 0 0);

                patchFields
                {
                    T
                    {
                        type        zeroGradient;
                    }

                    p
                    {
            			type        fixedFluxPressure;
                        gradient    uniform 0;
                        value       uniform 1e5;
                    }

                    p_rgh
                    {
            			type        fixedFluxPressure;
                        gradient    uniform 0;
                        value       uniform 1e5;
                    }

                    U
                    {
                        type        slip;
                    }

                    k
                    {
                        type        zeroGradient;
                    }

                    epsilon
                    {
                        type        zeroGradient;
                    }
                }
            }

            slave
            {
                //- Slave side patch
                name            bafflePool_slave;
                type            mappedWall;
                sampleMode      nearestPatchFace;
                sampleRegion    fluid;
                samplePatch     bafflePool_master;
                offsetMode      uniform;
                offset          (0 0 0);

                patchFields
                {
                    ${...master.patchFields}
                }
            }
        }
    }
}

// ************************************************************************* //
