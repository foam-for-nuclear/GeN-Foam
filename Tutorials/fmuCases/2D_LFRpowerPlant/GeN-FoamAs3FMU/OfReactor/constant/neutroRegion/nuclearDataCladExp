/*--------------------------------*- C++ -*----------------------------------*\
|   Cross-section dictionary                                                  |
|   Generated for GeN-Foam by SerpentToFoamXS Version 1.0 - 22/06/2022        |
|   Date: 22/06/2022                                                          |
|   From SERPENT results file: 2D_ALFRED_Critical_res.m                       |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     1.0;
    format      ascii;
    class       dictionary;
    location    "constant/neutroRegion";
    object      nuclearDataCladExp;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Effective delayed neutron fraction, prompt neutron spectrum

TCladRef                  763; // [K]

TCladPerturbed            813; // [K]


//- Parameters relevant only for Diffusion Neutronics

energyGroups              2; // from MACRO_NG

precGroups                1; // from LAMBDA


//- Foam zones from Serpent universes

zones
(/*
	innerFuel  //- Serpent Universe: uInnFuel
	{
		fuelFraction       1;
		IV                 nonuniform List<scalar> 2 ( 3.502040e-07 1.093140e-04 );
		D                  nonuniform List<scalar> 2 ( 1.174110e-02 6.640980e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 5.994990e-01 2.349940e+01 );
		sigmaPow           nonuniform List<scalar> 2 ( 6.811679e-12 2.700140e-10 );
		scatteringMatrixP0 2 2 (
			( 3.464720e+01 4.083980e-06 )
			( 2.927660e+00 4.407300e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 2.673180e+00 -9.900970e-07 )
			( -1.500870e-01 -1.787400e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.241710e+00 -4.447630e-08 )
			( -2.349980e-02 -7.412800e-01 )
		);
		scatteringMatrixP3 2 2 (
			( 4.704550e-01 -5.619900e-08 )
			( -2.142830e-01 3.713750e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 2.713950e-01 -4.832500e-08 )
			( -2.868140e-02 -8.416780e-01 )
		);
		scatteringMatrixP5 2 2 (
			( 7.464270e-02 -1.249130e-07 )
			( 1.487920e-01 -2.184860e-01 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.548000e-01 1.930500e+01 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 2.405381e-01 2.756681e-05 );
	}

	outerFuel  //- Serpent Universe: uOutFuel
	{
		fuelFraction       1;
		IV                 nonuniform List<scalar> 2 ( 3.900680e-07 1.316290e-04 );
		D                  nonuniform List<scalar> 2 ( 1.176510e-02 6.232450e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 6.978410e-01 8.630940e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 7.905573e-12 9.916987e-11 );
		scatteringMatrixP0 2 2 (
			( 3.467630e+01 1.089120e-04 )
			( 1.254680e+00 4.607100e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 2.744140e+00 -2.601850e-05 )
			( -1.331680e-01 7.222940e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.318740e+00 -1.919610e-06 )
			( -2.128090e-02 3.851440e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 5.022610e-01 -2.150680e-06 )
			( -3.728770e-02 2.332550e-01 )
		);
		scatteringMatrixP4 2 2 (
			( 2.921520e-01 1.040000e-06 )
			( 1.994290e-04 2.164560e-01 )
		);
		scatteringMatrixP5 2 2 (
			( 8.163160e-02 -6.559410e-09 )
			( -6.134850e-03 -3.374350e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 5.229000e-01 8.435000e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 2.433708e-01 1.154165e-03 );
	}

	innerFuelDiagrid  //- Serpent Universe: uDiagrid
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 5.007000e-06 1.298780e-04 );
		D                  nonuniform List<scalar> 2 ( 6.382350e-03 4.809670e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 6.204670e+01 1.945250e-02 )
			( 1.953020e+00 6.426980e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.147970e+00 -4.913150e-03 )
			( -1.626460e-01 8.043690e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.831420e-01 1.023990e-04 )
			( -3.925100e-02 6.696510e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 2.359290e-02 -4.228660e-04 )
			( -5.839660e-02 -5.507850e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 1.229610e-02 3.837710e-04 )
			( 7.476620e-03 3.009880e-02 )
		);
		scatteringMatrixP5 2 2 (
			( -9.523050e-03 -4.417400e-04 )
			( -2.056740e-02 -2.797120e-01 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.488000e-01 6.222200e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.837647e-04 4.422548e-04 );
	}

	outerFuelDiagrid  //- Serpent Universe: uDiagrid
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 5.007000e-06 1.298780e-04 );
		D                  nonuniform List<scalar> 2 ( 6.382350e-03 4.809670e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 6.204670e+01 1.945250e-02 )
			( 1.953020e+00 6.426980e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.147970e+00 -4.913150e-03 )
			( -1.626460e-01 8.043690e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.831420e-01 1.023990e-04 )
			( -3.925100e-02 6.696510e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 2.359290e-02 -4.228660e-04 )
			( -5.839660e-02 -5.507850e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 1.229610e-02 3.837710e-04 )
			( 7.476620e-03 3.009880e-02 )
		);
		scatteringMatrixP5 2 2 (
			( -9.523050e-03 -4.417400e-04 )
			( -2.056740e-02 -2.797120e-01 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.488000e-01 6.222200e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.837647e-04 4.422548e-04 );
	}

	outerDiagrid  //- Serpent Universe: uDiagrid
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 5.007000e-06 1.298780e-04 );
		D                  nonuniform List<scalar> 2 ( 6.382350e-03 4.809670e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 6.204670e+01 1.945250e-02 )
			( 1.953020e+00 6.426980e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.147970e+00 -4.913150e-03 )
			( -1.626460e-01 8.043690e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 1.831420e-01 1.023990e-04 )
			( -3.925100e-02 6.696510e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 2.359290e-02 -4.228660e-04 )
			( -5.839660e-02 -5.507850e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 1.229610e-02 3.837710e-04 )
			( 7.476620e-03 3.009880e-02 )
		);
		scatteringMatrixP5 2 2 (
			( -9.523050e-03 -4.417400e-04 )
			( -2.056740e-02 -2.797120e-01 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 4.488000e-01 6.222200e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.837647e-04 4.422548e-04 );
	}

	innerLowerFuelAssemblies  //- Serpent Universe: uInnLowerFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 1.002230e-06 1.184670e-04 );
		D                  nonuniform List<scalar> 2 ( 1.516480e-02 7.908930e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 2.642500e+01 3.583520e-04 )
			( 1.296280e+00 4.011780e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.111160e+00 -8.015940e-05 )
			( -1.917210e-01 3.907150e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.973240e-01 -9.907830e-06 )
			( -5.701810e-02 3.372140e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.266260e-01 -5.402740e-06 )
			( 6.263460e-03 -4.102610e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 6.934750e-02 -7.060160e-07 )
			( -2.376840e-03 1.944990e-01 )
		);
		scatteringMatrixP5 2 2 (
			( 8.581450e-03 4.421420e-06 )
			( -6.633870e-04 1.871540e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.209000e-01 3.043300e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 4.344914e-02 7.160447e-04 );
	}

	innerUpperFuelAssemblies  //- Serpent Universe: uInnUpperFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 2.095020e-06 1.198590e-04 );
		D                  nonuniform List<scalar> 2 ( 1.127660e-02 6.826810e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 3.526800e+01 2.135810e-03 )
			( 1.353350e+00 4.621190e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.155500e+00 -4.964030e-04 )
			( -1.919390e-01 6.305290e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.751350e-01 -4.542360e-05 )
			( -5.180340e-02 8.728440e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.111330e-01 -6.781360e-06 )
			( -1.737260e-02 6.367380e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.901570e-02 -9.572430e-06 )
			( -1.985780e-02 -1.365260e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 6.871330e-03 -8.394920e-07 )
			( -4.698000e-03 2.382200e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 8.930000e-02 3.085800e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 7.551432e-02 7.422079e-03 );
	}

	outerLowerFuelAssemblies  //- Serpent Universe: uOutLowerFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 1.457850e-06 1.260550e-04 );
		D                  nonuniform List<scalar> 2 ( 1.439510e-02 8.391100e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 2.773000e+01 1.823030e-03 )
			( 9.588010e-01 3.769350e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.164860e+00 -4.583810e-04 )
			( -1.436430e-01 4.341370e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 5.105780e-01 -3.225390e-05 )
			( -2.570070e-02 7.502330e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 1.304110e-01 -4.080040e-06 )
			( -2.042740e-02 8.680480e-03 )
		);
		scatteringMatrixP4 2 2 (
			( 7.340430e-02 -4.792950e-06 )
			( -5.468590e-03 1.372070e-02 )
		);
		scatteringMatrixP5 2 2 (
			( 9.995700e-03 -1.819790e-05 )
			( -6.984070e-03 1.839500e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 2.610000e-01 2.669100e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 3.579491e-02 4.039032e-03 );
	}

	outerUpperFuelAssemblies  //- Serpent Universe: uOutUpperFuelPlenum
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 2.352060e-06 1.214850e-04 );
		D                  nonuniform List<scalar> 2 ( 1.091390e-02 6.897350e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		sigmaPow           nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		scatteringMatrixP0 2 2 (
			( 3.612890e+01 2.920110e-03 )
			( 1.295630e+00 4.563940e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.159220e+00 -7.162980e-04 )
			( -1.845690e-01 4.627010e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.746370e-01 -4.365000e-05 )
			( -4.253080e-02 9.996640e-03 )
		);
		scatteringMatrixP3 2 2 (
			( 1.102130e-01 -5.449600e-06 )
			( -2.081140e-02 3.996000e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 5.899130e-02 3.176860e-06 )
			( -9.998080e-03 8.518160e-03 )
		);
		scatteringMatrixP5 2 2 (
			( 6.956850e-03 -1.604790e-05 )
			( -4.850320e-03 4.364830e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 9.850000e-02 2.998900e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 0.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 8.015637e-02 1.108375e-02 );
	}

	lowerReflectorAssemblies  //- Serpent Universe: uReflector
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 3.647170e-06 1.315770e-04 );
		D                  nonuniform List<scalar> 2 ( 8.967090e-03 6.827790e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 1.868040e-02 4.640900e-02 );
		sigmaPow           nonuniform List<scalar> 2 ( 2.125176e-13 5.333853e-13 );
		scatteringMatrixP0 2 2 (
			( 4.133250e+01 1.177550e-02 )
			( 1.052340e+00 4.643210e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.255610e+00 -2.883530e-03 )
			( -1.174220e-01 6.532250e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.711630e-01 -2.318680e-04 )
			( -4.601080e-02 7.933450e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 9.432270e-02 -7.478840e-05 )
			( -1.814680e-02 1.276370e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 4.971720e-02 -2.183380e-05 )
			( -7.085670e-03 2.762740e-03 )
		);
		scatteringMatrixP5 2 2 (
			( 7.920050e-03 -2.714910e-05 )
			( -3.443560e-03 1.793550e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.658000e-01 2.936900e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 1.613190e-01 9.600644e-02 );
	}

	middleReflectorAssemblies  //- Serpent Universe: uReflector
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 3.647170e-06 1.315770e-04 );
		D                  nonuniform List<scalar> 2 ( 8.967090e-03 6.827790e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 1.868040e-02 4.640900e-02 );
		sigmaPow           nonuniform List<scalar> 2 ( 2.125176e-13 5.333853e-13 );
		scatteringMatrixP0 2 2 (
			( 4.133250e+01 1.177550e-02 )
			( 1.052340e+00 4.643210e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.255610e+00 -2.883530e-03 )
			( -1.174220e-01 6.532250e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.711630e-01 -2.318680e-04 )
			( -4.601080e-02 7.933450e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 9.432270e-02 -7.478840e-05 )
			( -1.814680e-02 1.276370e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 4.971720e-02 -2.183380e-05 )
			( -7.085670e-03 2.762740e-03 )
		);
		scatteringMatrixP5 2 2 (
			( 7.920050e-03 -2.714910e-05 )
			( -3.443560e-03 1.793550e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.658000e-01 2.936900e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 1.613190e-01 9.600644e-02 );
	}

	upperReflectorAssemblies  //- Serpent Universe: uReflector
	{
		fuelFraction       0;
		IV                 nonuniform List<scalar> 2 ( 3.647170e-06 1.315770e-04 );
		D                  nonuniform List<scalar> 2 ( 8.967090e-03 6.827790e-03 );
		nuSigmaEff         nonuniform List<scalar> 2 ( 1.868040e-02 4.640900e-02 );
		sigmaPow           nonuniform List<scalar> 2 ( 2.125176e-13 5.333853e-13 );
		scatteringMatrixP0 2 2 (
			( 4.133250e+01 1.177550e-02 )
			( 1.052340e+00 4.643210e+01 )
		);
		scatteringMatrixP1 2 2 (
			( 1.255610e+00 -2.883530e-03 )
			( -1.174220e-01 6.532250e-01 )
		);
		scatteringMatrixP2 2 2 (
			( 4.711630e-01 -2.318680e-04 )
			( -4.601080e-02 7.933450e-02 )
		);
		scatteringMatrixP3 2 2 (
			( 9.432270e-02 -7.478840e-05 )
			( -1.814680e-02 1.276370e-02 )
		);
		scatteringMatrixP4 2 2 (
			( 4.971720e-02 -2.183380e-05 )
			( -7.085670e-03 2.762740e-03 )
		);
		scatteringMatrixP5 2 2 (
			( 7.920050e-03 -2.714910e-05 )
			( -3.443560e-03 1.793550e-02 )
		);
		sigmaDisapp        nonuniform List<scalar> 2 ( 1.658000e-01 2.936900e+00 );
		chiPrompt          nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		chiDelayed         nonuniform List<scalar> 2 ( 1.000000e+00 0.000000e+00 );
		Beta               nonuniform List<scalar> 6 ( 8.674870e-05 6.594970e-04 5.027290e-04 1.279090e-03 5.335910e-04 1.391140e-04 );
		lambda             nonuniform List<scalar> 6 ( 1.272010e-02 3.008180e-02 1.115760e-01 3.239660e-01 1.199480e+00 7.628920e+00 );
		discFactor         nonuniform List<scalar> 2 ( 1.000000e+00 1.000000e+00 );
		integralFlux       nonuniform List<scalar> 2 ( 1.613190e-01 9.600644e-02 );
	}
*/
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
