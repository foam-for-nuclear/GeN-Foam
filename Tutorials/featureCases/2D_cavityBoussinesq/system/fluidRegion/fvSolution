/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    arch        "LSB;label=32;scalar=64";
    class       dictionary;
    location    "system/fluidRegion";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p_rgh
    {
        solver          GAMG;
        smoother        DIC;
        tolerance       1e-06;
        relTol          0;
        maxIter         25;
    }
    p_rghFinal
    {
        solver          GAMG;
        smoother        DIC;
        tolerance       1e-06;
        relTol          0;
        maxIter         25;
    }
    "h.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-06;
        relTol          0;
        minIter         1;
    }
    ".*"
    {
        solver          PBiCGStab;
        preconditioner  diagonal;
        tolerance       1e-06;
        relTol          0.001;
    }
}

PIMPLE
{
    nOuterCorrectors    6;
    nCorrectors         12;
    correctUntilConvergence true;
    nNonOrthogonalCorrectors 0;
    momentumMode        faceCentered;
    oscillationLimiterFraction 0;
    solveEnergy         true;
    solveFluidMechanics true;
}

relaxationFactors
{
    equations
    {
    }
    fields
    {
    }
}


// ************************************************************************* //
