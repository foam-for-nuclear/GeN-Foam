#!/bin/bash

# --- Run from this directory
cd ${0%/*} || exit 1

# --- Initialize the output file
resFile=test.out
rm -rf $resFile
touch $resFile

# --- Run the simulation
echo -e "Run tutorial 1D_PSBT_SC ...\n" | tee -a $resFile
./Allrun >> log.simulation
echo -e "Done running tutorial 1D_PSBT_SC ...\n" | tee -a $resFile

# --- Check it ran
endString=$(tail -3 ./log.GeN-Foam | grep End)
if [ "$endString" = "End" ]; then
    echo -e "1D_PSBT_SC has converged \n" | tee -a $resFile
else
    echo -e "1D_PSBT_SC has NOT converged \n" | tee -a $resFile
    exit 1
fi

# --- Recover the results
input="./0.6/fluidRegion/alpha.vapour"
averageAt=$(head -98 "$input" | tail -1)
expectedAt=0.131886

# --- Check results are correct
isInRange()
{
    ref=$1
    value=$2
    error=$3
    valueMin=$(bc -l <<<"$value*(1-$error)")
    valueMax=$(bc -l <<<"$value*(1+$error)")
    if (( $(echo "$valueMin <= $ref && $ref <= $valueMax" | bc -l) )); then
        echo 0
    elif (( $(echo "$valueMax <= $ref && $ref <= $valueMin" | bc -l) )); then
        echo 0
    else
        echo 1
    fi
}

# --- Check that results are correct
error=0.05
echo "alpha.vapour at the end of the channel with $error relative error" | tee -a $resFile
if [ $(isInRange $expectedAt $averageAt $error) -eq 0 ]; then
    echo "Perfect match:" | tee -a $resFile
    echo "|              | Simulated | Expected |" | tee -a $resFile
    echo "|:-------------|:---------:|:--------:|" | tee -a $resFile
    echo "| alpha vapour | $averageAt | $expectedAt |" | tee -a $resFile
else
    echo "Divergent results:" | tee -a $resFile
    echo "|              | Simulated | Expected |" | tee -a $resFile
    echo "|:-------------|:---------:|:--------:|" | tee -a $resFile
    echo "| alpha vapour | $averageAt | $expectedAt |" | tee -a $resFile
    exit 1
fi

# --- Exit successfully
exit 0
